# Simple metaphlan3 workflows

Workflows using metaphlan3 and simple visualization of the results.

All examples presented were made for our TARS cluster system. This means you will be likely to find some
absolute path into the `config.yaml` that you might not have access to.

For every workflow, an example is provided and is based on the `config.yaml` file. Singularity images are necessary for these examples.
