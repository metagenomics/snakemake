configfile: "config.yaml"

# ==== Snakefile paths ==== 
__metaphlan2_rules = config.get("snakefiles", {}).get("metaphlan2")
__metaphlan2_merge_rules = config.get("snakefiles", {}).get("metaphlan2_merge")
__metaphlan2_heatmap_rules = config.get("snakefiles", {}).get("metaphlan2_heatmap")
__graphlan_from_metaphlan2_rules = config.get("snakefiles", {}).get("graphlan_from_metaphlan2")

__input_dir = config['input_dir']
__main_output_dir = config.get('output_dir', 'output')

# ---- Metaphlan2
__metaphlan2_output_dir = __main_output_dir + "/metaphlan2"
__metaphlan2_input_type = config['metaphlan2'].get('input_type', 'fastq')
__metaphlan2_input = "{dir}/{sample}.{ext}".format(dir=__input_dir,
                                                   sample="{sample}",
                                                   ext=__metaphlan2_input_type + ".gz")
__metaphlan2_output = "{dir}/{sample}.txt".format(dir=__metaphlan2_output_dir,
                                                  sample="{sample}")
include: __metaphlan2_rules

# ---- Metaphlan2 merge
__metaphlan2_merge_output_dir = __main_output_dir + "/metaphlan2_merge"
__metaphlan2_merge_output_file_name = config['metaphlan2_merge'].get('output_file_name',"merged_taxonomic_profiles.txt")
__metaphlan2_merge_input = expand("{dir}/{sample}.txt".format(dir=__metaphlan2_output_dir,
                                                              sample="{sample}"),
                                  sample=config['samples'])
__metaphlan2_merge_output = "{dir}/{file_name}".format(dir=__metaphlan2_merge_output_dir,
                                                       file_name=__metaphlan2_merge_output_file_name) 
include: __metaphlan2_merge_rules

# ---- Metaphlan2 heatmap
__metaphlan2_heatmap_output_dir = __main_output_dir + "/metaphlan2_heatmap"
__metaphlan2_heatmap_output_file_name = config['metaphlan2_heatmap'].get('output_name',"heatmap.png")
__metaphlan2_heatmap_input = __metaphlan2_merge_output
__metaphlan2_heatmap_output = "{dir}/{file_name}".format(dir=__metaphlan2_heatmap_output_dir,
                                                         file_name=__metaphlan2_heatmap_output_file_name) 
include: __metaphlan2_heatmap_rules

# ---- Graphlan Dendogram
__graphlan_from_metaphlan2_output_dir = __main_output_dir + "/graphlan"
__graphlan_from_metaphlan2_output_file_name =  config.get("graphlan_from_metaphlan2", {}).get('output_name',"dendrogram.png")
__graphlan_from_metaphlan2_input = __metaphlan2_merge_output
__graphlan_from_metaphlan2_output = "{dir}/{file_name}".format(dir=__graphlan_from_metaphlan2_output_dir,
                                                         file_name=__graphlan_from_metaphlan2_output_file_name) 
include: __graphlan_from_metaphlan2_rules

rule all:
    input:
        heatmap = __metaphlan2_heatmap_output,
        dendogram = __graphlan_from_metaphlan2_output
