# StrainPhlan

This describes a workflow to run [strainphlan](https://github.com/biobakery/MetaPhlAn/wiki/StrainPhlAn-3.0)

In brief it contains 4 steps:

* Run Metaphlan on all samples (paired-ends)
* Extract markers from metaphlan outputs
* Run StrainPhlan on all selected clades