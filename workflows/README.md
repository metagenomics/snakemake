# Snakemake workflows

This directory contains every workflows built from Snakemake tool and subworkflows found in the `tools` and `subworkflows` directories of the repository.

## Design and best practices

All the Snakemake workflows try to respect some rules and best practices in their design.
Therefore, workflows have the following parts:

1. Reference to options from a `config.yaml` file.
2. Snakefile paths
3. Set up of common variable for the workflow
4. Set up of specific variables for the tools
5. `rule all` to specify what is/are the specific file(s) expected from the workflow.

> **Note**: These workflows are not made to be fully portable and reusable but at least give around 80% of the work done. You might need to do minor modifications. The idea being to have a copy of `Snakefile` workflow with its dedicated `config.yaml` file for each experiments. 

### Reference to options from `config.yaml` file

First part correspond to all options that are set up from a `config.yaml` file. They all have the nomenclature `__TOOLNAME_variable`.

Then in the YAML `config.yaml` file, you can set the variable as followed:

```yaml
TOOLNAME:
  variable: 4
```

We recommend to use the `get` method to access this parameter and set a default value:

```python
config['TOOLNAME'].get('variable', 1)
```

### Snakefile paths

All `Snakefile` path can be configured in the `config.yaml` file but have default value that are relative path in this repository.

### Set up common variable for the workflow

Then every common variables are set up. This can be handy for reference to a common directory.

### Set up specific variables for the tools

Every specific variables for every tools are then specified. For more details about the way
every tool is describe, you can refer to the `README` of the `tools` directory of the
repository.

In brief, every tool has its input and output refered as a variable: `__TOOLNAME_variable`.
Therefore every variable needs to be configured and we recommand using reference to a `config.yaml` file.
Thus, we can easily set up the input of a tool `B` as being the output of `A`:

```python
# ---- Tool A
__toola_input = config.get('input')
__toola_output = "file.a"

# ---- Tool B
__toolb_input = __toola_output
__toolb_ouput = config.get('output')
```

## Usage

### Using on TARS

For the following examples, we consider that we want to run every job a dedicated queue called `atm`.

First need a virtual environment with snakemake:

```bash
module load Python/3.6.0  # Install seems to fail with latest version of Python
python3 -m venv .snakemake_venv
source .snakemake_venv/bin/activate
(.snakemake_venv)$ pip install snakemake
```

Then, to run a `SnakeFile` on Tars we use a command similar to the following:

```bash
sbatch --qos=atm -p atm -c 1 snakemake -p -j 6 --cluster-config cluster.yml --cluster "sbatch --qos={cluster.queue} -p {cluster.queue} -c {threads}"
```

For cluster configuration, you need to have a `cluster.yml` file on the running directory.