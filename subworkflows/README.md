# Subworkflows

Similar to tools, subworkflows cannot be used directly but need to be called in a higher Snakefile
by setting up the required variables.

## Design and best practices

All subworkflows are described in a `Snakefile` in a directory having its name.
All Snakefiles try to respect some rules and best practices in their design:

* Snakefile paths
* Set up different parameters for tool rules

### Snakefile paths

All `Snakefile` path can be configured in the `config.yaml` file but have default value that are relative path in this repository.

### Set up different parameters for tool rules

Then for every tool, it should have some input and output to specify. (_ref to tools directory README for more information_).

As for the tool descriptions, input and output for subworkflows are abstracted using the nomenclature `__SUBWORKFLOW_NAME_input` and `__SUBWORKFLOW_NAME_output`.

For intermediate tools, you can then easily link output and input:

```python
__tool1_output = __subworkflow_output + ".tool1.out"
__tool2_input = __tool1_input
```