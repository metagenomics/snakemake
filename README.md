# Snakemake tools and workflows

This repository aims to gather Snakemake descriptions. They all try to follow some rules in order to make them easily reusable from one workflow to another.

Thus, the repository is splitted in three sections:

* [__tools__](tools/): All tool descriptions. These are not meant to be used directly and need to be refered in another Snakefile.
* [__subworkflows__](subworkflows/): Supposed to behave as tool descriptions, meaning that they need to be refered in another Snakefile to be used.
* [__workflows__](workflows/): All workflows that are supposed to be usable directly.