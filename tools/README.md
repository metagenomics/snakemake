# Tools

This directory contains all tool descriptions that can be imported and used within Snakemake workflows.

## Design and best practices

All tools are described in a `Snakefile` in a directory having its name.
All Snakefiles try to respect some rules and best practices in their design:

* Reference to options from a `config_example.yaml` file
* Tool description

### Reference to options from `config.yaml` file

First part correspond to all options that are set up from a `config.yaml` file. They all have the nomenclature `__TOOLNAME_variable`.

Then in the YAML `config_example.yaml` file, you can set the variable as followed:

```yaml
TOOLNAME:
  variable: 4
```

We recommend to use the `get` method to access this parameter and set a default value:

```python
config['TOOLNAME'].get('variable', 1)
```

### Tool description

In order to ease the linking of tools in a workflow, every parts of the tool is described as followed:

* input with the nomenclature: `__TOOLNAME_input`
* output with the nomenclature: ` __TOOLNAME_output`
* params with different options that are described above
  * There is usually a `exec_command` to give the possibility to change the way the tool is called (locally installed, singularity ...)
  * There is usually a `options` to specify all other command line options. _You can still give a more detailed level of description for the options_
* the shell command

input and output are then set up in the workflow `Snakefile` that refer to the rule.
Therefore the rules cannot be used directly.

> **Info**: You have noticed the possibility to give `modules` this is dedicated to our HPC that have some tools accessible via `module`.

## Example

To find out example, refer to the workflow based on this tool descriptions in the `/workflows/` section.
