# sample2markers.py for strainphlan

This step will reconstruct all species strains found in metaphlan output sam file and store them in a pickle file (*.pkl). Those strains are referred as sample-reconstructed strains.

.. **Note**: output pkl file is written should be written in a individual directory since the script tries
    to create and write into a `tmp` dir that leads to error when running sample2markers.py in parallel.

### Help section

```
usage: sample2markers.py [-h] [-i INPUT [INPUT ...]] [--sorted]
                         [-f INPUT_FORMAT] [-o OUTPUT_DIR]
                         [-b BREADTH_THRESHOLD] [-n NPROCS]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT [INPUT ...], --input INPUT [INPUT ...]
                        The input samples as SAM or BAM files
  --sorted              Whether the BAM input files are sorted. Default false
  -f INPUT_FORMAT, --input_format INPUT_FORMAT
                        The input samples format {bam, sam, bz2}. Default bz2
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        The output directory
  -b BREADTH_THRESHOLD, --breadth_threshold BREADTH_THRESHOLD
                        The breadth of coverage threshold for the consensus
                        markers. Default 80 (%)
  -n NPROCS, --nprocs NPROCS
                        The number of threads to execute the script
```
