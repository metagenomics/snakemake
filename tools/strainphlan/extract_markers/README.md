# extract_markers.py for strainphlan

This step will extract the markers of selected species from MetaPhlAn database.

### Help section

```
usage: extract_markers.py [-h] [-d DATABASE] [-c CLADE] [-o OUTPUT_DIR]

optional arguments:
  -h, --help            show this help message and exit
  -d DATABASE, --database DATABASE
                        The input MetaPhlAn dtabase
  -c CLADE, --clade CLADE
                        The clades to investigate
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        The output directory
```
